#include<iostream>

void spellStr(char* str){
	str++; // it is legal to change the address of a pointer
	std::cout << std::endl;
	for(; *str!=0; ++str)
		std::cout << *str << std::endl;
	// this will print the str w/o the first element
	// because of the initial increment str=str+1.
}

int main(){
	float v[] {5.6,2.,8,0};
	char s[] {"trento"};

	for(float j: v)
		std::cout << j << std::endl;

	for(int j: v)
		std::cout << j << std::endl;
	
	/* this fails because array is not a pointer;
	so ++s == (s=s+1) is illegal for s is a
	statically allocated memory block and cannot
	be moved. What you can do is pass the char 
	s[] array to a routine by reference 
	(in which a pointer is declared, see 
	void spellStr) and increment there by ++ in
	the loop.
	
	for(; *s!=NULL; ++s)
		std::cout << *s << std::endl;
	*/
	spellStr(s);
}
