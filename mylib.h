/* --------------------------------------------------------------------
Sorting routines
-------------------------------------------------------------------- */
// Bubble sort
template<class T> void srtBubble(T w[], int LEN){
        /*
        Loop over array position, compare 2-by-2 and swap to sort;
        in the second loop the upper bound is (DIM-1)-(counter of first loop)
        so no need to check for the already placed values; a flag quit the loops
        if no swap ever occurred.
        Complexity: O(n^2)
        */
        bool anyswap;
        int kk=0;
        do{
                anyswap=false;
                for(int jj=0; jj<LEN-1-kk; jj++){
                        if(w[jj]>w[jj+1]){
                                swap(w,jj,jj+1);
                                anyswap=true;
                        }
                }
                kk++;
        }while(anyswap==true);
}

// Selection sort
template<class T> void srtSelect(T w[], int LEN){
        /*
        The array is filled one at a time with the minimum
        of each sub-array.
        Complexity: O(n^2)
        */
        for(int ii=0; ii<LEN-1; ii++){
                int jj=0;
                jj=min_ind(w,ii,LEN);
                swap(w,ii,jj);
        }
}

// Insert sort
template<class T> void srtInsert(T v[], T w[], int LEN){
        for(int ii=0; ii<LEN; ii++){
                /*
                Run the index kk untill the number
                to be placed is smaller than the one already present
                or the sub-array (0 to ii) is over.
                Complexity: O(n^2)
                */
                int kk=0;
                while((v[ii]>w[kk]) && (kk<ii))
                        kk++;

                shiftR(w,kk,ii); // shift to the right all the other numbers already there
                w[kk]=v[ii]; // set the value at the now free position
        }
}

// Quicksort
template<class T> void srtQuick(T w[], int left, int right){
        /*
        Partition does the job, that is comparisons, swaps and sub-array splitting;
        then srtQuick is called recursively for each sub-array (left-hand visiting).
        */
        int q; // partition index

        if(left < right) // check if the array dimension is not a singleton
        {
                // partition: swap untill (right < left) then split array
                q = partition(w,left,right);
                // recursive calls for left and right sub-arrays
                srtQuick(w,left,q-1);
                srtQuick(w,q+1,right);
        }
}

// Partition for Quick sorter
template<class T> int partition(T w[],int left, int right){
        int pvt, idx_pvt; // value and index of pivot
        idx_pvt=left; // set pivot
        pvt=w[idx_pvt];
        // swap untill (right<left)
        while(left<right){
                while((w[left]<=pvt) && (left<right)){ left++;} // increase left untill value>pivot or end of array
                while(w[right]>pvt) { right--;} // increase right untill value<=pivot (no need to check for array dim since pivot value is accepted to halt the while)
                if(left<right){
                        swap(w,left,right);
                        // these two lines below are required to account for any swap of the pivot before left>right happens, this occurs when pivot is not the first element!!!
                        if(left==idx_pvt){ idx_pvt=right;}
                        if(right==idx_pvt){ idx_pvt=left;}
                }
        }
        // now swap pivot with element at index right and return the latter
        swap(w,idx_pvt,right);
        return right;
}

/* --------------------------------------------------------------------
Searching routines
-------------------------------------------------------------------- */
// Linear search
template<class T> int srcLinear(T v[], T num, int LEN){
        /*
        Look for the element by scanning the array. As soon as the num is found,
        a flag signals to the do/while to exit the loop. If flag never turned true
        the num was not found.
        Complexity: avg(search at each n position) = (n(n+1)/2 ) / n = O(n).

        The algorithm can exploit an additional flag iff sorted (!), that is
        while((kk<DIM AND found==false) OR v[kk]>num).
        */
        bool found=false;
        int kk=0;

        do{
                if(v[kk]==num)
                        found=true;
                else
                        kk++;

        }while((found==false) && (kk<LEN));
        
	if(found==true)
                cout << "num = " << num << " is at " << kk << "-th position" << endl;
        else
                cout << "num = " << num << " not found" << endl;

}

// Binary search
template<class T> int srcBinary(T v[], int init, int last, T num){
        /* Sorting of the array, divide et impera using middle value, go to left sub-array
        if searched num < v[middle] or go to right sub-array on the other hand.
        Complexity: number of steps O(log(n))
        */
        int mid;
        if(init<last){
                mid=(init+last)/2;
                if(v[mid]==num)
                        return mid;
                else{
                        if(num>v[mid])
                                return srcBinary(v,mid+1,last,num);
                        else
                                return srcBinary(v,init,mid-1,num);
                }
        }
        else{ return -1;}
}


/* --------------------------------------------------------------------
Utility routines
-------------------------------------------------------------------- */

// Find index of minimum in array for given boundaries, for Selection sorter
template<class T> int min_ind(T w[],int init, int last){
        int idx=init;
        for(int kk=init; kk<last; kk++){
                if(w[kk]<w[idx]){ idx=kk;}
        }
        return idx;
}

// Fill in a 1-dim array with pseudo-random numbers in range given by {MAX,SHIFT}.
template<class T> void fillVectRand(T v[], int LEN, int MAX, int SHIFT){
	srand(time(0));
	for(int j=0; j<LEN; j++){
		v[j] = (rand()/MAX) + SHIFT;
	}
}

// Right shift for Insert sorter
template<class T> void shiftR(T w[], int init, int last){
        /*
        Shift from where I want to put the new number to the end of the sub-array,
        this has to be made from the bottom of the sub-array otherwise you're
        going to overwrite the following elements.
        Complexity: O(n^2)
        */
        for(int jj=last-1; jj>=init; jj--){
                w[jj+1] = w[jj];
        }
}

// Copy v into w
template<class T> void copyarr(T v[], T w[], int LEN){
        for(int ii=0; ii<LEN; ii++)
                w[ii] = v[ii];

}

// Swap ii-th and jj-th elements of array, for Selection sorter
template<class T> void swap(T w[], int a, int b){
        int tmp;
        tmp  = w[b];
        w[b] = w[a];
        w[a] = tmp;
}

// Routine to print out results onto terminal
template<class T> void printOut(T v[], int LEN, string title){
        cout << title << endl;
        for(int ii=0; ii<LEN; ii++){
                cout << v[ii] << " ";
        }
        cout << endl;
}
