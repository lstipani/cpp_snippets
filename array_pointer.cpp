#include<iostream>
#include<ctime>
using namespace std;

int main(){
	const int LEN=6;
	
	int v[LEN];
	
	v[0]=1;
	cout << v[0] << endl;
	cout << *v << endl;

	v[1]=2;
	cout << v[1] << endl;	
	cout << *(v+1) << endl;
	
	*(v+2)=3;
	cout << v[2] << endl;	
	cout << *(v+2) << endl;
}
