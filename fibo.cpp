#include<iostream>

template<typename T> void Fibonacci_iter(T* v, std::size_t n){
	*(v+0) = 0;
	*(v+1) = 1;
	for(std::size_t i=2; i<=n; i++)
		*(v+i) = *(v+i-1) + *(v+i-2);
}


template<typename T> unsigned long Fibonacci_rec(T n){
	if(n==0)
		return 0;
	else if(n==1)
		return 1;
	else
		return Fibonacci_rec(n-1) + Fibonacci_rec(n-2);		
}

template<typename T> void show(T* v, std::size_t n){
	for(size_t i=0; i<=n; i++)
		std::cout << *(v+i) << "\t";
	std::cout << std::endl;
}

int main(){
	const std::size_t N = 10;
	unsigned int fibo[N+1];
	
	Fibonacci_iter(fibo, N);
	show(fibo, N);

	for(std::size_t i=0; i<=N; i++)
		fibo[i] = Fibonacci_rec(i);
	show(fibo, N);
}
