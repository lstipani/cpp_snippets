#include <iostream>
using namespace std;

template<typename T> unsigned long Fact(T n){
	return (n==0) ? 1 : n*Fact(n-1);
}

template<typename T> unsigned long doubFact(T n){
	return (n==0 || n==1) ? 1 : n*doubFact(n-2);
}

template<typename T> unsigned long Fact_iter(T n){
	unsigned long fact = 1;
	for(size_t i=n-1; i>0; --i)
		fact = fact*(i+1);
	return fact;
}

int main(){
	unsigned int x;
	
	// input from user
	cout << "provide a positive integer" << endl;
	cin >> x;	
	
	// evaluation and print out
	cout << "the factorial is " << Fact(x) << endl;
	cout << "the (iterative) factorial is " << Fact_iter(x) << endl;
	cout << "the double factorial is " << doubFact(x) << endl;
}

