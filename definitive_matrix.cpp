/*


*/

#include<iostream>

const int N = 3;
const int M = 4;

// c++ natural way, safe, no loss of size information
template<class T, size_t rows, size_t cols> void showMatrix_template(T (&mat)[rows][cols]){
	for(std::size_t j=0; j<rows; ++j){
		for(std::size_t i=0; i<rows; ++i){
			std::cout << mat[i][j] << "\t";
		}
		std::cout << std::endl;
	}
}

// c-style, pass a pointer to the 2-dim array, the two dimensions must be known! 
template<class T> void showMatrix_pointer(T (*mat)[N][M]){
	for(std::size_t j=0; j<M; ++j){
		for(std::size_t i=0; i<N; ++i){
			std::cout << (*mat)[i][j] << "\t"; // deferenciate pointer and then access
		}
		std::cout << std::endl;
	}
}

// pass a pointer to rows of M T-variables, number of rows must be known to iterrate!
template<class T> void showMatrix_array(T (*mat)[M], size_t rows){
	for(std::size_t j=0; j<M; ++j){
		for(std::size_t i=0; i<rows; ++i){
			std::cout << mat[i][j] << "\t";
		}
		std::cout << std::endl;
	}
}

/*
given T array[N][M], 
array[i][j] (with i<N, j<M) is short notation for *(array + j + M*i)
that's why you need to pass the number of columns, because C/C++
functions take only pointers not arrays (aka array decayed to pointer)
and the second dimension is required to loop over consecutive blocks
of stack memory.
*/

int main(){
	const std::size_t NR = N;
	const std::size_t NC = M;
	float matrix[NR][NC];
	
	
	for(std::size_t r=0; r<NR; ++r){
		for(std::size_t c=0; c<NC; ++c)
			matrix[r][c] = (float) rand()%2;
	}

	showMatrix_template(matrix); std::cout << std::endl;
	showMatrix_pointer(&matrix); std::cout << std::endl;
	showMatrix_array(&matrix[0], NR); std::cout << std::endl;

}
