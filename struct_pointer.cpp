#include<iostream>
using namespace std;

struct libro{
		int id;
		const char *title;
	};

void changeTitle(libro* p, const char* str){
	p->title = str;
}

int main(){
	
	const char _str[] = "guerra e pace";
	libro item {1,_str};
	libro *p_item;
	
	p_item = &item;
		
	cout << p_item->title << endl;
	
}

/*
char *p = "foo"

equivale a

char unnamed[] = "foo"
char *p = unnamed

*/
