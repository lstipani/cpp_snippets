#include<iostream>

using namespace std;

int main(){
	const char* str1 = "ciao";
	char str2[5] = "ciao";
	
	cout << str2[3] << endl; // questo stampa una o
	cout << str2[4] << endl; // questo stampa EOL
	
	// tutti modi validi di accedere alla stringa via puntatore
	cout << *str1 << endl;
	cout << *(str1+1) << endl;
	cout << str1[2] << endl;
}
