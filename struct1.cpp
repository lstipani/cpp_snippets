#include<iostream>
using namespace std;

struct Libro{
	const char *title;
	int id;
};

void setTitle(Libro* _p, const char* _title){
	_p->title = _title;
}

int main(){
	
	Libro a {"bibbia",1};
	Libro* p = &a;
	
	setTitle(p, "LaBibbia");
	cout << p->title << endl;
}
