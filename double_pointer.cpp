/*
Double pointer to int
*/

#include<iostream>

int main(){
	int x=1;
	int *p;
	int **pp;
	
	p = &x;
	pp = &p;
	std::cout << **pp << std::endl;
}
