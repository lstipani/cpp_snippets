#include<iostream>
#include<string>
using namespace std;

class Book{
	public:
		string title;
		string author;
		int isbn;
		
		Book(string title, string author, string text){
			Book::title=title;
			Book::author=author;
			Book::text=text;
			cout << title << " created" << endl;
		}
		void readAloud();
	private:
		int id_archive;
		string text;
};

void Book::readAloud(){
	string str = Book::text;
	cout << "*****\n I am about to read the book with ISBN = " << Book::isbn << endl;
	cout << "ehm ehm... silence please... and... let's go" << endl;
	cout << str << endl;
}

int main(){
	
	Book *sample = new Book("You and me", "Me", "Lorem ipsum quack quack");
	sample->isbn = 6781;
	(*sample).readAloud(); //sample->readAloud();
}
