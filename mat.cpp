#include<iostream>
#include<cmath>
#include<ctime>

using namespace std;
template<typename T> void fillArr(T* v);

// matrix dimensions
int DIM1=5;
int DIM2=3;
// pseudorandom range
const int MAX=100;
const int SHIFT=1;


int main(){

	float A[DIM1][DIM2];

	fillArr(A);
	cout << A[1][2] << endl;
}

template<class T> void fillArr(T* v){
	srand(time(NULL));
	for(int i=0; i<DIM1; i++){
		for(int j=0; j<DIM2; j++){
			v[i][j] = (rand()/MAX) + SHIFT;
		}
	}
}
