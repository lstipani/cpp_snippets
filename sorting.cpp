#include<iostream>
#include<cstdlib>
#include<ctime>
#include<chrono>
#define DIM 10 // array dimension

using namespace std;
using namespace std::chrono;
void srtInsert(int v[], int w[]);
void shiftR(int w[], int init, int last);
void srtSelect(int v[], int w[]);
int min_ind(int w[], int init, int last);
void srtBubble(int v[], int w[]);
void swap(int w[], int a, int b);
void printOut(int arr[], string title);

int main(){
	int v[DIM]; // unsorted array
	int w[DIM]; // sorted array

	// initialize the two array, the first with random numbers in (0,50), the second with zeros
	srand(time(0));
	for(int ii=0; ii<DIM; ii++){
		*(v+ii)=(rand())%50;
		*(w+ii)=0;
	}
	
	// print out original randomly-filled array
	printOut(v,"Original");

	/*
	call sorting routines and print out results + time of execution
	*/
	
	// insert sorting
	auto start=high_resolution_clock::now();
	srtInsert(v,w);
	auto stop=high_resolution_clock::now();
	auto duration=duration_cast<nanoseconds>(stop-start);
	printOut(w,"Insert sorting: ");
	cout << "Exec time: " << duration.count() << "ns" << "\n =====" << endl;

	// selection sorting
	start=high_resolution_clock::now();
	srtSelect(v,w);
	stop=high_resolution_clock::now();
	duration=duration_cast<nanoseconds>(stop-start);
	printOut(w,"Selection sorting: ");
	cout << "Exec time: " << duration.count() << "ns" << "\n =====" << endl;
	
	// bubble sorting
	start=high_resolution_clock::now();
	srtBubble(v,w);
	stop=high_resolution_clock::now();
	duration=duration_cast<nanoseconds>(stop-start);
	printOut(w,"Bubble sorting: ");
	cout << "Exec time: " << duration.count() << "ns" << "\n =====" << endl;
	
}

// Insert sorter
void srtInsert(int v[], int w[]){
	for(int ii=0; ii<DIM; ii++){
		/* 
		run the index kk untill the number
		to be placed is smaller than the one already present
		or the sub-array (0 to ii) is over.
		*/
		int kk=0;
		while((v[ii]>w[kk]) && (kk<ii)){
			kk++;
		}
		
		shiftR(w,kk,ii); // shift to the right all the other numbers already there
		w[kk]=v[ii]; // set the value at the now free position
	}	
}

// Right shift for Insert sorter
void shiftR(int w[], int init, int last){
	/*
	shift from where I want to put the new number to the end of the sub-array,
	this has to be made from the bottom of the sub-array otherwise you're
	going to overwrite the following elements.
	*/
	for(int jj=last-1; jj>=init; jj--){
		w[jj+1]=w[jj];
	}
}

// Selection sorter
void srtSelect(int v[], int w[]){
	for(int ii=0; ii<DIM; ii++){ w[ii]=v[ii];} // copy v into w
	
	/*
	the array is filled one at a time with the minimum 
	of each sub-array.
	*/
	for(int ii=0; ii<DIM-1; ii++){
		int jj=0;
		jj=min_ind(w,ii,DIM);
		swap(w,ii,jj);
	}
}

// Find index of minimum in array for given boundaries, for Selection sorter
int min_ind(int w[],int init, int last){
	int idx=init;
	for(int kk=init; kk<last; kk++){
		if(w[kk]<w[idx]){ idx=kk;}
	}
	return idx;
}

// Swap ii-th and jj-th elements of array, for Selection sorter
void swap(int w[], int a, int b){
	int tmp;
	tmp=w[b];
	w[b]=w[a];
	w[a]=tmp;
}

// Bubble sorter
void srtBubble(int v[], int w[]){
	for(int ii=0; ii<DIM; ii++){ w[ii]=v[ii];} // copy v into w

	/*
	loop over array position, compare 2-by-2 and swap to sort;
	in the second loop the upper bound is (DIM-1)-(counter of first loop)
	so no need to check for the already placed values; a flag quit the loops
	if no swap ever occurred.
	*/
	bool anyswap;
	int kk=0;
	do{
		anyswap=false;
		for(int jj=0; jj<DIM-1-kk; jj++){
			if(w[jj]>w[jj+1]){
				swap(w,jj,jj+1);
				anyswap=true;
			}
		}
		kk++;
	}while(anyswap==true);
}

// Routine to print out results onto terminal
void printOut(int arr[], string title){
	cout << title << endl;
	for(int ii=0; ii<DIM; ii++){
		cout << arr[ii] << " ";
	}
	cout << endl;
}
