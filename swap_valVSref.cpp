#include <iostream>
using namespace std;

//void swapRef(int *a, int *b);
void swapRef(int &a, int &b);
void swapVal(int a, int b);

int main(){
	int x=3;
	int y=9;
	
//	swapRef(&x,&y);
	swapRef(x,y);
	cout << "after swapping via REF: x = " << x << " and y = " << y << endl;
	
	int xx=3;
	int yy=9;
	swapVal(xx,yy);
	cout << "after swapping via VAL: x = " << xx << " and y = " << yy << endl;
	
}
/*
void swapRef(int *a, int *b){
	int tmp;
	 
	tmp  = *a;
	*a   = *b;
	*b   = tmp;
}
*/
void swapRef(int &a, int &b){
	int tmp;
	 
	tmp = a;
	a   = b;
	b   = tmp;
}
void swapVal(int a, int b){
	int tmp;
	
	tmp = a;
	a   = b;
	b   = tmp;
}
